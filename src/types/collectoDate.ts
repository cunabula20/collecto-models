export type ERA = 'BCE' | 'CE';

export type CollectoDate = {
  year: number;
  month?: number;
  date?: number;
};
