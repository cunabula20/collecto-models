import { ValueField } from './valueField';

export type ValueLinks = ValueField & {
  linkIds: string[] | null;
};
