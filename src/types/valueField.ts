import { FIELD_TYPE } from './typeField';

export type ValueField = {
  typeFieldId: string;
  type: FIELD_TYPE;
  title: string;
};
