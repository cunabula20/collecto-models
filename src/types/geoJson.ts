export type GeoJson = {
  type: string;
  geometry: {
    type: string;
    coordinates: [number, number];
  };
};
