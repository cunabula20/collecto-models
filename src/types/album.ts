export type Album = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  title: string;
  desc?: string;
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
  unitSharedFolder?: boolean;
};
