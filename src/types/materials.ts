export interface Materials {
  /**
   * Id and also the file name (UUID v4) of the file in Cloud Storage.
   */
  id: string;
  /**
   * Date that represents when the file was created.
   * This value is set by the client, so it could be different from the time it was actually recorded in the database.
   */
  createTime?: Date;
  /**
   * Date that represents when the file was update.
   * This value is set by the client, so it could be different from the time it was actually recorded in the database.
   */
  updateTime?: Date;
  /**
   * Custom editable name of the file.
   */
  fileName: string;
  /**
   * Custom string that identifies the file type. It can be viewed as a file extension rather than a mime type.
   * However, it's neither a mime type nor a file extension.
   * Example values: "pdf", "cvs", "png".
   */
  fileType: string;
  /**
   * Duration in seconds. Applies to audio and video files.
   */
  duration?: number;
  /**
   * File size in bytes.
   */
  fileSize?: number;
  /**
   * Local file object. Set only if the file has not been uploaded yet.
   * For entries that come from the database this property will never be set.
   */
  __fileObject?: File;
}
