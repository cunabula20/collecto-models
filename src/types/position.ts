export type Point = {
  lat: number;
  lng: number;
};

export type Position = {
  point: Point;
  floorLevel: number;
};
