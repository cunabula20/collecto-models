import { TypeField } from './typeField';

export type TypeNumber = TypeField & {
  properties?: {
    units?: string[];
  };
};
