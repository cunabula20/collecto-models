export type UploadFiles = {
  id: string;
  createTime?: any;
  updateTime?: any;
  fileName: string;
  fileType: string;
};
