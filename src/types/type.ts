import { Section } from './section';

export const SYSTEM_OWNER_ID = 'SYSTEM';

export type Type = {
  id: string;
  createTime?: any;
  updateTime?: any;
  title: string;
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
  imageFileId?: string;
  sections: Section[];
  locked?: boolean;
};
