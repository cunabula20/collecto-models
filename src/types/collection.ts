export type Collection = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  title: string;
  desc?: string;
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
};
