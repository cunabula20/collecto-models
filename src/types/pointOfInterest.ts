import { Position } from './position';

export enum POI_TYPE {
  TOILET = 'TOILET',
  CAFE = 'CAFE',
  SHOP = 'SHOP',
  EXIT = 'EXIT',
}

export type PointOfInterest = {
  type: POI_TYPE;
  position: Position;
};
