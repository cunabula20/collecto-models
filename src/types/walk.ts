import { TranslatedField } from './translatedField';

export type Walk = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  organisationId: string;
  unitId: string;
  ownerId: string;
  itemIds: string[];
  name: TranslatedField<string>;
  description?: TranslatedField<string>;
  isSpotlight: boolean;
  isArchived?: boolean;
  locationId?: string;
  liveStartDate: Date;
  liveEndDate: Date;
  duration?: TranslatedField<number>; // sum of all audio and video files in seconds per language
  imageFileId?: string;
  albumIds: string[];
};
