export type Address = {
  id?: string;
  street?: string;
  houseNumber?: string;
  city?: string;
  postCode?: string;
  country?: string;
};
