import type { ValueField } from './valueField';

/**
 * An object that represents a file that is either already uploaded or not yet uploaded.
 *
 * In the case of an un-uploaded (local) file, the `__fileObject` property will be defined.
 * If the file is already uploaded, `__fileObject` will be undefined.
 */
export interface CollectoFile {
  /**
   * Id and also the file name (UUID v4) of the file in Cloud Storage.
   */
  id: string;
  /**
   * File size in bytes.
   */
  fileSize: number;
  /**
   * Custom string that identifies the file type. It can be viewed as a file extension rather than a mime type.
   * However, it's neither a mime type nor a file extension.
   * Example values: "pdf", "cvs", "png".
   */
  fileType: string;
  /**
   * Date that represents when the file was created.
   * This value is set by the client, so it could be different from the time it was actually recorded in the database.
   */
  createTime: Date;
  /**
   * Date that represents when the file was updated.
   * This value is set by the client, so it could be different from the time it was actually recorded in the database.
   */
  updateTime: Date;
  /**
   * Custom editable name of the file.
   */
  fileName: string;
  /**
   * Local file object. Set only if the file has not been uploaded yet.
   * For entries that come from the database this property will never be set.
   */
  __fileObject?: File;
}

/**
 * Generic interface that represents both new and old (pre- PDMA-134) shapes of a file field.
 */
type GenericValueFile = ValueField & {
  /**
   * Exists ony for backward compatibility. Use `files` instead.
   * @deprecated
   */
  storageFileId: string;
  /**
   * Exists ony for backward compatibility. Use `files` instead.
   * @deprecated
   */
  fileName: string;
  /**
   * Exists ony for backward compatibility. Use `files` instead.
   * @deprecated
   */
  fileType: string;

  /**
   * Collection of files attached to the file field.
   * Added as part of PDMA-134. Before PDMA-134 each file field could only support one file.
   * If this property is present, it means the file field uses the latest format.
   * If `files` is missing, it means the file field uses the legacy format.
   */
  files?: CollectoFile[];
};

/**
 * A file field. If you're certain that the field has the latest shape (post- PDMA-134), use simply `ValueFile`.
 * If unsure, use `ValueFile<true>` to indicate it's a possible legacy format that needs to be inspected manually.
 */
export type ValueFile<IsLegacy = false> = IsLegacy extends true ? GenericValueFile : Required<GenericValueFile>;
