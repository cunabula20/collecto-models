import { TypeField } from './typeField';

export enum LINK_TYPE {
  ARTISTS = 'ARTISTS',
  BRAND = 'BRAND',
  LOCATION = 'LOCATION',
}

export type TypeLink = TypeField & {
  properties?: {
    type: LINK_TYPE;
  };
};
