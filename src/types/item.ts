import { ValueField } from './valueField';
import { Materials } from './materials';
import { UploadFiles } from './uploadFiles';
import { CollectoDate } from './collectoDate';
import { TranslatedField } from './translatedField';

export enum IMAGE_SIZE {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
  ORIGINAL = 'original',
}

export type ArtistInfo = {
  caption?: string;
  copyrightInfo?: string;
};

export type Item = {
  id: string;

  title: string;
  description?: string;
  translatedDesc?: TranslatedField<string>;
  earliestCreatedDate?: CollectoDate;
  latestCreatedDate?: CollectoDate;
  dateAcquired?: Date;
  collectionId: string | null;
  albumIds: string[];
  imageFileIds?: string[];
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
  createTime?: Date;
  updateTime?: Date;
  fields?: ValueField[];
  typeId: string;
  artistIds: string[] | null;
  brandId: string | null;
  locationId: string | null;
  roomId?: string;
  materials?: Materials[];
  sharedUnitFolder?: boolean;
  uploadFiles?: UploadFiles[];
  searchTags?: { [key: string]: string | string[] };
  imageDetails?: { [fileId: string]: ArtistInfo | undefined };
  showOnlyInWifiFence?: boolean;
  additionalContentTitle?: TranslatedField<string>;
  additionalContentUrl?: TranslatedField<string>;
  /**
   * Unique client-controlled value. Normally a timestamp in milliseconds.
   */
  version?: string;
  /**
   * Id of the user who last updated the item.
   */
  lastUpdatedBy?: string;
};
