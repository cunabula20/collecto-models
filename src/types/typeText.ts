import { TypeField } from './typeField';

export type TypeText = TypeField & {
  properties?: {
    multiline?: boolean;
  };
};
