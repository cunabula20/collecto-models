import { ValueField } from './valueField';

export type ValueLink = ValueField & {
  linkId: string | null;
};
