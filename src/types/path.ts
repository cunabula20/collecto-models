type PathPoint = [number, number, number];

export type Path = {
  path: PathPoint[];
  weight: number;
  edgeDatas: any;
};
