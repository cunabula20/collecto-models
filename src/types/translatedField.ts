export enum LANG_KEYS {
  DE = 'de',
  DE_SIMPLE = 'de_simple',
  EN = 'en',
  EN_SIMPLE = 'en_simple',
  FR = 'fr',
  FR_SIMPLE = 'fr_simple',
}

export type TranslatedField<T> = {
  [key in LANG_KEYS]?: T;
};
