import { TypeField } from './typeField';

export type TypeDate = TypeField & {
  properties?: {
    minYear?: number;
    maxYear?: number;
  };
};
