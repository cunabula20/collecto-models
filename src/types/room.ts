export type Room = {
  id: string;
  floor: number;
  name: string;
  imageFileId?: string;
  filePlanMarkerName?: string;
};
