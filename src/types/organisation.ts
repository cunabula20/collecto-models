import { LANG_KEYS, TranslatedField } from './translatedField';

export enum COLLECTO_PACKAGES {
  RAPID_CAPTURE = 'RAPID_CAPTURE',
  SET_UP = 'SET_UP',
  SET_UP_AND_PRINTS = 'SET_UP_AND_PRINTS',
}

export type Unit = {
  id: string;
  name: TranslatedField<string>;
};

export type Organisation = {
  id: string;
  name: string;
  updateTime: any;
  createTime: any;
  imageId: string;
  package: COLLECTO_PACKAGES;
  units: Unit[];
  defaultLang: LANG_KEYS;
  wifiFencingActive?: boolean;
  wifiFencingSSIDs?: string[];
  mainLocation?: string;
  helpDocsUrl?: string;
  analyticsEmbedUrl?: string;
};
