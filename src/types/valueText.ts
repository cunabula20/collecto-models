import { ValueField } from './valueField';

export type ValueText = ValueField & {
  value: string;
};
