export type Brand = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  name: string;
  desc?: string;
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
};
