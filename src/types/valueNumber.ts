import { ValueField } from './valueField';

export type ValueNumber = ValueField & {
  value: number;
  selectedUnit?: string;
};
