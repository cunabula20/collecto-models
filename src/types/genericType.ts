import { SYSTEM_OWNER_ID, Type } from './type';
import { FIELD_TYPE } from './typeField';
import { LINK_TYPE } from './typeLink';

export const GENERIC_TYPE_ID = 'GENERIC';

export const genericType: Type = {
  id: GENERIC_TYPE_ID,
  ownerId: SYSTEM_OWNER_ID,
  sections: [
    {
      title: 'Generic',
      id: 'GENERIC',
      fields: [
        {
          id: 'ARTISTS',
          title: 'Artists',
          type: FIELD_TYPE.LINK,
          properties: {
            type: LINK_TYPE.ARTISTS,
          },
        },
        {
          id: 'BRAND',
          title: 'Brand',
          type: FIELD_TYPE.LINK,
          properties: {
            type: LINK_TYPE.BRAND,
          },
        },
      ],
    },
  ],
  title: 'Generic',
};
