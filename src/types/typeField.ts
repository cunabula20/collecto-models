export enum FIELD_TYPE {
  NUMBER = 'NUMBER',
  TEXT = 'TEXT',
  FILE = 'FILE',
  LINK = 'LINK',
  DATE = 'DATE',
}

export type TypeField = {
  id: string;
  type: FIELD_TYPE;
  order?: number;
  title: string;
  properties?: any;
  isPublic?: boolean;
};
