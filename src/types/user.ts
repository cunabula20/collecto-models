export enum USER_ROLES {
  SUPER_ADMIN = 'SUPER_ADMIN',
  OWNER = 'OWNER',
  ADMIN = 'ADMIN',
  EDITOR = 'EDITOR',
  GUEST = 'GUEST',
}

export type User = {
  id: string;
  username: string;
  email: string;
  updateTime: any;
  createTime: any;
  searchApiKey: string;
  organisationId?: string;
  language?: string;
  role?: USER_ROLES;
  unitId?: string;
};
