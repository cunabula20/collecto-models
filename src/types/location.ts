import { Address } from './address';
import { Room } from './room';
import { TranslatedField } from './translatedField';

export type Location = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  name: string;
  desc?: string;
  translatedDesc?: TranslatedField<string>;
  address?: Address;
  lowestFloor: number;
  highestFloor: number;
  imageFileId?: string;
  floorPlanFiles?: FloorPlanFile[];
  rooms: Room[];
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
};

export type FloorPlanFile = {
  floor: number;
  imageFileId?: string;
};
