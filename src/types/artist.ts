export type Artist = {
  id: string;
  createTime?: Date;
  updateTime?: Date;
  firstName?: string;
  lastName: string;
  desc?: string;
  ownerId?: string;
  organisationId?: string;
  unitId?: string;
};
