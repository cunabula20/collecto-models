import { ValueField } from './valueField';
import { CollectoDate } from './collectoDate';

export type ValueDate = ValueField & {
  earliestDate?: CollectoDate;
  latestDate?: CollectoDate;
};
