import { TypeField } from './typeField';
import { Subsection } from './subsection';

export type Section = {
  id: string;
  title: string;
  order?: number;
  subsections?: Subsection[];
  fields?: TypeField[];
};
