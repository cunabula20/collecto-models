import { TypeField } from './typeField';

export type TypeFile = TypeField & {
  properties?: {
    allowedExtensions?: string[];
  };
};
