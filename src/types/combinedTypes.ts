import { Brand } from './brand';
import { Artist } from './artist';
import { Album } from './album';
import { Collection } from './collection';
import { Type } from './type';
import { Location } from './location';

export type CombinedTypes = Collection | Album | Type | Artist | Brand | Location;
