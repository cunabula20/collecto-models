import { TypeField } from './typeField';

export type Subsection = {
  id: string;
  title: string;
  order?: number;
  fields: TypeField[];
};
